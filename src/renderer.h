#pragma once

class IRenderable
{
public:
	virtual char GetSymbol() const = 0;
};

class IRenderer
{
public:
	virtual void Draw(int iX, int iY, IRenderable *pRenderable) = 0;
	virtual void Clear() = 0;
	virtual void Present() const = 0;
};