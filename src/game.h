#pragma once

#include <vector>
#include "renderer.h"

#define FIELD_WIDTH 15
#define FIELD_HEIGHT 15

class CGameObject;

class IGameObjectManager
{
public:
	virtual void AddObject(CGameObject *pObj) = 0;
	virtual void RemoveObject(CGameObject *pObj) = 0;
	virtual void GetObjectsAtCell(int iX, int iY, std::vector<CGameObject *> &vecObjs) = 0;
};

class CGame : private IGameObjectManager
{
	IRenderer *_pRenderer;

	std::vector<CGameObject *> _vecObjects, _vecRemovePendingObjs;

	void _StartGame();
	void _ClearGame();

	void AddObject(CGameObject *pObj);
	void RemoveObject(CGameObject *pObj);
	void GetObjectsAtCell(int iX, int iY, std::vector<CGameObject *> &vecObjs);

public:

	CGame(IRenderer *pRenderer);
	~CGame();

	void Step();
};