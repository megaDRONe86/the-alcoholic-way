#include <assert.h>
#include <algorithm>
#include "game.h"

// CGameObject
class CGameObject : public IRenderable
{
public:

	enum E_GAME_OBJECT_TYPE { GOT_UNKNOWN, GOT_BACKGROUND, GOT_ALCOHOLIC, GOT_TAVERN, GOT_COLUMN, GOT_BOTTLE };

protected:

	IGameObjectManager *_pObjMan;
	E_GAME_OBJECT_TYPE _eObjType;
	int _iX, _iY, _iZ;
	unsigned _uCounter;

public:

	CGameObject(IGameObjectManager *pObjMan) : _pObjMan(pObjMan), _eObjType(GOT_UNKNOWN), _iX(0), _iY(0), _iZ(0), _uCounter(0) {}
	
	CGameObject(IGameObjectManager *pObjMan, int iX, int iY, int iZ, E_GAME_OBJECT_TYPE eType):
	_pObjMan(pObjMan), _eObjType(eType), _iX(iX), _iY(iY), _iZ(iZ), _uCounter(0) {}

	virtual ~CGameObject() {}

	static bool s_CompareFunction(CGameObject *obj1, CGameObject *obj2)
	{
		return (obj1->_iZ < obj2->_iZ);
	}

	inline int X() const { return _iX; }
	inline int Y() const { return _iY; }

	E_GAME_OBJECT_TYPE GetType() const
	{
		return _eObjType;
	}
	
	char GetSymbol() const
	{
		switch (_eObjType)
		{
		case GOT_UNKNOWN: return ' ';
		case GOT_BACKGROUND: return '.';
		case GOT_TAVERN: return 'T';
		case GOT_COLUMN: return 'C';
		case GOT_BOTTLE: return 'b';
		default:
			assert(false);
			return ' ';
		}
	}

	virtual void Step()
	{
		++_uCounter;
	}
	
	virtual bool CollisionCheck(CGameObject *pObjWith)
	{
		return _iX == pObjWith->_iX && _iY == pObjWith->_iY;
	}

	virtual void OnCollision(CGameObject *pObjWith) {}

	virtual bool IsCollidable() const
	{
		return false;
	}
};

// helper function
void RemoveOccupiedCells(std::vector<CGameObject *> &vecObjs, std::vector<CGameObject::E_GAME_OBJECT_TYPE> &vecExceptions)
{
	std::vector<CGameObject *> objs(vecObjs);
	
	vecObjs.clear();

	for (size_t i = 0; i < objs.size(); ++i)
		if (!objs[i]->IsCollidable() || std::find(vecExceptions.begin(), vecExceptions.end(), objs[i]->GetType()) != vecExceptions.end())
		{
			bool flag = false;
			
			for (size_t j = 0; j < objs.size(); ++j)
				if (i != j &&
					objs[j]->IsCollidable() && objs[i]->X() == objs[j]->X() && objs[i]->Y() == objs[j]->Y() &&
					std::find(vecExceptions.begin(), vecExceptions.end(), objs[i]->GetType()) == vecExceptions.end())
				{
					flag = true;
					break;
				}

			if (!flag)
				vecObjs.push_back(objs[i]);
		}
}

// CBottle
class CBottle : public CGameObject
{
public:
	CBottle(IGameObjectManager *pObjMan, int iX, int iY) : CGameObject(pObjMan, iX, iY, 1, GOT_BOTTLE) {}
	bool IsCollidable() const { return true; }
};

// CAlcoholic
class CAlcoholic : public CGameObject
{
	std::vector<CGameObject::E_GAME_OBJECT_TYPE> _vecCanCollideWith;
	int _iPrevX, _iPrevY;
	bool _bSleeping, _bStanding;
	unsigned _uBottlesCounter;

public:
	
	CAlcoholic(IGameObjectManager *pObjMan, int iX, int iY):
		CGameObject(pObjMan, iX, iY, 2, GOT_ALCOHOLIC),
		_iPrevX(iX), _iPrevY(iY),
		_bSleeping(false), _bStanding(true),
		_uBottlesCounter(1)
	{
		_vecCanCollideWith.push_back(GOT_COLUMN);
		_vecCanCollideWith.push_back(GOT_BOTTLE);
		_vecCanCollideWith.push_back(GOT_ALCOHOLIC);
		_uCounter = 5;
	}
	
	bool IsCollidable() const { return true; }

	char GetSymbol() const
	{
		return _bStanding ? 'D' : 'd';
	}

	void LeaveOnlyBGsAndSleepingAlcogolics(std::vector<CGameObject *> &vecObjs)
	{
		std::vector<CGameObject *>::iterator iter = vecObjs.begin();
		
		while (iter != vecObjs.end())
			if ((*iter)->GetType() != GOT_BACKGROUND &&
				((*iter)->GetType() == GOT_ALCOHOLIC && !((CAlcoholic *)(*iter))->_bSleeping)
				)
				iter = vecObjs.erase(iter);
			else
				++iter;
	}

	void Step()
	{
		CGameObject::Step();

		if (_uCounter > 5 && _bSleeping)
		{
			_bSleeping = false;
			_bStanding = true;
		}

		if (!_bSleeping)
		{
			std::vector<CGameObject *> objs_around;

			// our possible moves
			_pObjMan->GetObjectsAtCell(_iX + 1, _iY, objs_around);
			_pObjMan->GetObjectsAtCell(_iX - 1, _iY, objs_around);
			_pObjMan->GetObjectsAtCell(_iX, _iY + 1, objs_around);
			_pObjMan->GetObjectsAtCell(_iX, _iY - 1, objs_around);

			RemoveOccupiedCells(objs_around, _vecCanCollideWith); // leave only available moves

			LeaveOnlyBGsAndSleepingAlcogolics(objs_around);

			if (!objs_around.empty())
			{
				const size_t idx = rand() % objs_around.size();
				
				_iPrevX = _iX;
				_iPrevY = _iY;
				
				_iX = objs_around[idx]->X();
				_iY = objs_around[idx]->Y();

				if (rand() % 6 == 0 && _uBottlesCounter > 0)
				{
					--_uBottlesCounter;
					_pObjMan->AddObject(new CBottle(_pObjMan, _iPrevX, _iPrevY));
				}
			}
		}
	}

	void OnCollision(CGameObject *pObjWith)
	{
		switch (pObjWith->GetType())
		{
		case GOT_ALCOHOLIC: // may be only with sleeping alcoholic
			if (!_bSleeping)
			{
				_bStanding = ((CAlcoholic *)pObjWith)->_bStanding;
				_bSleeping = true;
				
				_uCounter = 0;
				_iX = _iPrevX;
				_iY = _iPrevY;
			}
			break;

		case GOT_BOTTLE:
			_bSleeping = true;
			_bStanding = false;
			_uCounter = 0;
			++_uBottlesCounter;
			_pObjMan->RemoveObject(pObjWith);
			break;

		case GOT_COLUMN:
			_bSleeping = true;
			_uCounter = 0;
			_iX = _iPrevX;
			_iY = _iPrevY;
			break;
		}
	}
};

// CColumn
class CColumn : public CGameObject
{
public:
	CColumn(IGameObjectManager *pObjMan, int iX, int iY) : CGameObject(pObjMan, iX, iY, 2, GOT_COLUMN) {}
	bool IsCollidable() const { return true; }
};

// CTavern
class CTavern : public CGameObject
{
	std::vector<CGameObject::E_GAME_OBJECT_TYPE> _vecCanCreateOn;

public:
	CTavern(IGameObjectManager *pObjMan, int iX, int iY):
		CGameObject(pObjMan, iX, iY, 2, GOT_TAVERN)
	{
		_vecCanCreateOn.push_back(GOT_BOTTLE);
	}
	
	bool IsCollidable() const { return true; }

	void Step()
	{
		if (_uCounter % 20 == 0)
		{
			std::vector<CGameObject *> objs_around;

			for (int i = -1; i < 2; ++i)
				for (int j = -1; j < 2; ++j)
					_pObjMan->GetObjectsAtCell(i + _iX, j + _iY, objs_around);

			RemoveOccupiedCells(objs_around, _vecCanCreateOn);

			if (!objs_around.empty())
			{
				const size_t idx = rand() % objs_around.size();
				_pObjMan->AddObject(new CAlcoholic(_pObjMan, objs_around[idx]->X(), objs_around[idx]->Y()));
			}
		}

		CGameObject::Step();
	}
};

CGame::CGame(IRenderer *pRenderer) : _pRenderer(pRenderer)
{
	_StartGame();
}

CGame::~CGame()
{
	_ClearGame();
}

void CGame::_StartGame()
{
	for (unsigned i = 0; i < FIELD_WIDTH; ++i)
		for (unsigned j = 0; j < FIELD_HEIGHT; ++j)
			AddObject(new CGameObject(this, i, j, 0, CGameObject::GOT_BACKGROUND));

	AddObject(new CColumn(this, 7, 7));
	AddObject(new CTavern(this, 0, 0));
}

void CGame::_ClearGame()
{
	for (size_t i = 0; i < _vecObjects.size(); ++i)
		delete _vecObjects[i];

	_vecObjects.clear();
	_vecRemovePendingObjs.clear();
}

void CGame::AddObject(CGameObject *pObj)
{
	_vecObjects.push_back(pObj);
}

void CGame::RemoveObject(CGameObject *pObj)
{
	_vecRemovePendingObjs.push_back(pObj);
}

void CGame::GetObjectsAtCell(int iX, int iY, std::vector<CGameObject *> &vecObjs)
{
	for (size_t i = 0; i < _vecObjects.size(); ++i)
		if (_vecObjects[i]->X() == iX && _vecObjects[i]->Y() == iY)
			vecObjs.push_back(_vecObjects[i]);
}

void CGame::Step()
{
	// update objects state
	size_t objs_count = _vecObjects.size();

	for (size_t i = 0; i < objs_count; ++i)
		_vecObjects[i]->Step();

	// do collision check
	objs_count = _vecObjects.size(); // save object count not to iterate through objects being created in collision handlers
	
	for (size_t i = 0; i < objs_count; ++i)
		if (_vecObjects[i]->IsCollidable())
			for (size_t j = 0; j < objs_count; ++j)
				if (_vecObjects[j]->IsCollidable() && i != j && _vecObjects[i]->CollisionCheck(_vecObjects[j]))
					_vecObjects[i]->OnCollision(_vecObjects[j]);

	// remove objects waiting to be removed
	for (size_t i = 0; i < _vecRemovePendingObjs.size(); ++i)
		for (size_t j = 0; j < _vecObjects.size(); ++j)
			if (_vecObjects[j] == _vecRemovePendingObjs[i])
			{
				delete _vecObjects[j];
				_vecObjects.erase(_vecObjects.begin() + j);
				break;
			}
	
	_vecRemovePendingObjs.clear();

	// sort objects by render layer before drawing
	std::sort(_vecObjects.begin(), _vecObjects.end(), CGameObject::s_CompareFunction);

	// draw game objects
	
	_pRenderer->Clear();
	
	for (size_t i = 0; i < _vecObjects.size(); ++i)
		_pRenderer->Draw(_vecObjects[i]->X(), _vecObjects[i]->Y(), _vecObjects[i]);
	
	_pRenderer->Present();
}