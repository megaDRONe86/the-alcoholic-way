#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "console_renderer.h"

CConsoleRenderer::CConsoleRenderer(unsigned uBasisX, unsigned uBasisY, unsigned uFieldWidth, unsigned uFieldHeight):
	_c_uBufferLength((uFieldWidth + 1) * uFieldHeight + 1),
	_uWidth(uFieldWidth), _uHeight(uFieldHeight),
	_uBasisX(uBasisX), _uBasisY(uBasisY)
{
	assert(_uBasisX < _uWidth && _uBasisY < _uHeight);
	_pcBuffer = new char[_c_uBufferLength];	
}

CConsoleRenderer::~CConsoleRenderer()
{
	delete[] _pcBuffer;
}

void CConsoleRenderer::Draw(int iX, int iY, IRenderable *pRenderable)
{
	const int y = iY + (int)_uBasisY;

	assert(iX < (int)_uWidth && y < (int)_uHeight && iX + _uBasisX >= 0 && y >= 0);
	
	_pcBuffer[(iX + (int)_uBasisX + (y != 0 ? 1 : 0)) + (iY + (int)_uBasisY) * ((int)_uWidth + 1)] = pRenderable->GetSymbol();
}

void CConsoleRenderer::Clear()
{
	for (unsigned i = 0; i < _c_uBufferLength - 1; ++i)
		if (i != 0 && i % (_uWidth + 1) == 0)
			_pcBuffer[i] = '\n';
		else
			_pcBuffer[i] = ' ';

	_pcBuffer[_c_uBufferLength - 1] = '\0';

#if defined(_WIN32) || defined(_WIN64)
	system("cls");
#else
	system("clear");
#endif
}

void CConsoleRenderer::Present() const
{
	printf("%s\n", _pcBuffer);
}