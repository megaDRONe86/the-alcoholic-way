#include <ctime>
#include <stdio.h>
#include "console_renderer.h"
#include "game.h"

#if defined(_WIN32) || defined(_WIN64)
#	include <Windows.h>
#else
#   include <stdlib.h>
#   include <unistd.h>
#endif

int main(int argc, char* argv[])
{
	srand((unsigned)time(0));

	CConsoleRenderer rndr(1, 1, FIELD_WIDTH + 2, FIELD_HEIGHT + 2); // add 1 unit border around logic field
	CGame game(&rndr);

	for (unsigned i = 0; i < 200; ++i)
	{
		game.Step();

#if defined(_WIN32) || defined(_WIN64)
		Sleep(50);
#else
		usleep(50 * 1000);
#endif

		printf("%s%u", "Step:", i);
	}

	return 0;
}
