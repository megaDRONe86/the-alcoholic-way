#pragma once

#include "renderer.h"

class CConsoleRenderer : public IRenderer
{
private:

	const unsigned _c_uBufferLength;

	unsigned _uWidth, _uHeight, _uBasisX, _uBasisY;
	
	char *_pcBuffer;

public:

	CConsoleRenderer(unsigned uBasisX, unsigned uBasisY, unsigned uFieldWidth, unsigned uFieldHeight);
	~CConsoleRenderer();

	void Draw(int iX, int iY, IRenderable *pRenderable);
	void Clear();
	void Present() const;
};
